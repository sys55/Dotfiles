function befcom (){
   pexrett="Exited with $? within $(exec_time)s at \A."  # prompt + execution time + return value + time
}

function commons
{
	source $HOME/.commonrc
}

function preexec(){
		com_preexec
}

function precmd(){
		com_precmd
}

function specific # these are shell specific. things from commans can be overwritten here.
{

	# dir aliases
	alias 1='cd -'
	alias 2='cd -2'
	alias 3='cd -3'
	alias 4='cd -4'
	alias 5='cd -5'
	alias 6='cd -6'
	alias 7='cd -7'
	alias 8='cd -8'
	alias 9='cd -9'
	
	setopt nonomatch
	setopt re_match_pcre
	setopt EXTENDED_GLOB

    # antigen plugin manager
    source ~/.zsh/antigen.zsh

	antigen use oh-my-zsh

	# bundles from default repo
	antigen bundle git
	antigen bunlde command-not-found
	antigen bundle docker
	
	# bundles from external repos
	antigen bundle pszynk/zsh-dircolors.git
    antigen bundle zsh-user/zsh-completions
	antigen bundle zsh-users/zsh-autosuggestions
	antigen bundle zsh-users/zsh-syntax-highlighting

    antigen apply

	# completions
	zstyle ':completion:*' auto-description 'specify: %d'
	zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
	zstyle ':completion:*' file-sort access
	zstyle ':completion:*' format 'Completing type: %d:'
	zstyle ':completion:*' group-name ''
	zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
	zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
	zstyle ':completion:*' matcher-list 'r:|[. - .]=* r:|=*' 'm:{[:lower:]}={[:upper:]}'
	zstyle ':completion:*' max-errors 3 numeric
	zstyle ':completion:*' menu select=1
	zstyle ':completion:*' prompt 'Error found: %e. Suggestions:'
	zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s, number: %1
	zstyle ':completion:*' squeeze-slashes true
	zstyle ':completion:*' use-compctl false
	zstyle ':completion:*' verbose true
	zstyle :compinstall filename '/home/sys55/.zshrc'

	autoload -Uz compinit
	compinit

	# autocd
	setopt  autocd autopushd \ pushdignoredups

	export HIST FILE="$HOME/.zhistory"    # History filepath
	export HISTSIZE=10000                   # Maximum events for internal history
	export SAVEHIST=10000                   # Maximum events in history file
	
	export ZSH_DIRCOLORS_DIR="/home/sys55/.zsh/dircolors"

	# completion
	autoload -U compinit; compinit
	_comp_options+=(globdots) # With hidden files

	# colored GCC warnings and errors
	export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

	# Enable colors 
	autoload -U colors && colors

	# Basic auto/tab complete:
	autoload -U compinit
	zstyle ':completion:*' menu select
	zmodload zsh/complist
	compinit
	_comp_options+=(globdots)		# Include hidden files.

	## syntax highlighting
    #source ~/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

	## enabling more complicated completions
	fpath=(~/.zsh/plugins/zsh-completions/src $fpath)

	## enabling bd
    . $HOME/.zsh/plugins/bd/bd.zsh
	autoload -Uz bd; bd

	# vi mode
	bindkey -v
	export KEYTIMEOUT=1

	# Use vim keys in tab complete menu:
	bindkey -M menuselect 'h' vi-backward-char
	bindkey -M menuselect 'k' vi-up-line-or-history
	bindkey -M menuselect 'l' vi-forward-char
	bindkey -M menuselect 'j' vi-down-line-or-history
	bindkey -v '^?' backward-delete-char

	# Change cursor shape for different vi modes.
	function zle-keymap-select {
	  if [[ ${KEYMAP} == vicmd ]] ||
	     [[ $1 = 'block' ]]; then
	    echo -ne '\e[1 q'
	  elif [[ ${KEYMAP} == main ]] ||
	       [[ ${KEYMAP} == viins ]] ||
	       [[ ${KEYMAP} = '' ]] ||
	       [[ $1 = 'beam' ]]; then
	    echo -ne '\e[5 q'
	  fi
	}
	zle -N zle-keymap-select
	zle-line-init() {
	    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
	    echo -ne "\e[5 q"
	}
	zle -N zle-line-init
	echo -ne '\e[5 q' # Use beam shape cursor on startup.
	preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
	
	# Use lf to switch directories and bind it to ctrl-o
	lfcd () {
	    tmp="$(mktemp)"
	    lf -last-dir-path="$tmp" "$@"
	    if [ -f "$tmp" ]; then
	        dir="$(cat "$tmp")"
	        rm -f "$tmp"
	        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
	    fi
	}
	bindkey -s '^o' 'lfcd\n'

	# Edit line in vim with ctrl-e:
	autoload edit-command-line; zle -N edit-command-line
	bindkey '^e' edit-command-line


	source ~/.zsh/.antigen/bundles/pszynk/zsh-dircolors/zsh-dircolors.plugin.zsh
	


	# window title
	autoload -Uz add-zsh-hook

	function xterm_title_precmd () {
				print -Pn -- '\e]2;%n@%m %~\a'"$TERM"
					[[ "$TERM" == 'screen'* ]] && print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-}\e\\'
			}

	function xterm_title_preexec () {
				print -Pn -- '\e]2;%n@%m %~ %# '"$TERM" && print -n -- "${(q)1}\a"
					[[ "$TERM" == 'screen'* ]] && { print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-} %# '"$TERM" && print -n -- "${(q)1}\e\\"; }
			}

	if [[ "$TERM" == (Eterm*|alacritty*|aterm*|gnome*|konsole*|kterm*|putty*|rxvt*|screen*|tmux*|xterm*) ]]; then
				add-zsh-hook -Uz precmd xterm_title_precmd
					add-zsh-hook -Uz preexec xterm_title_preexec
	fi

	eval "$(starship init zsh)"

}


befcom
commons
specific
startup

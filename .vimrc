let mapleader = " " " map leader to Space

" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding. 
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END


function! GitStatus()
  	let [a,m,r] = GitGutterGetHunkSummary()
	return printf('+%d ~%d -%d', a, m, r)
endfunction


" highlight SignColumn guibg=whatever ctermbg=whatever



" }}}


" startup
set nocompatible   " Disable compatibility with vi which can cause unexpected issues.              
filetype on        " Enable type file detection. 
filetype plugin on " Enable plugins and load plugin for the detected file type.
set spell spelllang=en_us 
set autoread 	   " If there are external changes happened to the file during editing (such as being edited by another editor), a prompt will be given.

" errors
set visualbell " when an error occures, a visual clue will be given

" encoding
set encoding=utf-8
set t_Co=256

" indentation
set autoindent
set tabstop=4
filetype indent on

" status bar
set ruler
set showcmd " show commands

" statusline
set statusline=  								" Clear status line when vimrc is reloaded.  
set statusline+=\ %F\ %M\ %Y\ %R 						" Status line left side.
set statusline+=%{GitStatus()}
set statusline+=%= 								" Use a divider to separate the left side from the right side.      
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%% 	" Status line right side.
set laststatus=2 								" Show the status on the second to last line.

" line numbers
set number 
set relativenumber

" apperance
syntax on
set background=dark
colorscheme molokai
set cursorline
set cursorcolumn

" search
set incsearch " While searching though a file incrementally highlight matching characters as you type.                                       
set showmatch " show matching words during typing
set hlsearch  " highlighted search

" history 
set history=1000

" Etc 
set wildmenu 							       		  " Enable auto completion menu after pressing TAB.                                
set wildmode=list:longest 							  " Make wildmenu behave like similar to Bash completion.       
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx 	  " Wildmenu will ignore files with these extensions.

" cursor
autocmd VimEnter * silent exec "! echo -ne '\e[1 q'" 
let &t_SI = "\e[5 q"   
let &t_EI = "\e[2 q" 


" MAPPINGS --------------------------------------------------------------- {{{

" sudo macro
cmap w!! w !sudo tee % >/dev/null

" Press \\ to jump back to the last cursor position.
nnoremap <leader>\ ``  


" navigate through splitted tabs with ctrl + w + hjkl 
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l 

" resizing the splits
nnoremap <C-w>a :vertical resize +5<CR>
nnoremap <C-w>d :vertical resize -5<CR>

" changing beginning/end of the words
nnoremap E ea
nnoremap B bi
nnoremap W wi

" un/capitialize one character
nnoremap gb gul
nnoremap gB gUl

" inserting new lines
map <C-o> o<Esc>k 
map <C-<ALT-o>> O<Esc>j 

" duplicating a line
nnoremap <C-p> yyp

" tab in normal mode inserts tab
nnoremap <TAB> i<TAB><Esc>

nnoremap Y y$
" nnoremap S d$a No need for this, because we already have C 


"" plugins

" toggle gitgutter
nnoremap <Leader>g :GitGutterToggle<CR> 

" toggle NERDtree
nnoremap <Leader>t :NERDTreeToggle<CR>

" next error
nnoremap <Leader>e :ALENext<CR>
nnoremap <Leader>z :ALEPrevious<CR>

" detailed error message
nnoremap <C-q> :ALEDetail<CR>

" english to turkish
nmap <silent> <Leader>tt <Plug>Translate --target_lang=Turkish
vmap <silent> <Leader>tt <Plug>TranslateV  --target_lang=Turkish
" turkish to english
nmap <silent> <Leader>te <Plug>Translate  --target_lang=English
vmap <silent> <Leader>te <Plug>TranslateV  --target_lang=English
" " Replace the text with translation
" nmap <silent> <Leader>r <Plug>TranslateR
" vmap <silent> <Leader>r <Plug>TranslateRV
" " Translate the text in clipboard
" nmap <silent> <Leader>x <Plug>TranslateX


" }}}


" STATUS LINE ------------------------------------------------------------ {{{



" }}}
"

" PACKAGE MANAGER ---------------------------------------------------------------- {{{

" package manager
execute pathogen#infect('packages/{}')

" }}}
"

" PLUGIN CONFIGURATION ---------------------------------------------------------------- {{{

" gitgutter
set foldtext=gitgutter#fold#foldtext()
highlight GitGutterAdd    guifg=#009900 ctermfg=2
highlight GitGutterChange guifg=#bbbb00 ctermfg=3
highlight GitGutterDelete guifg=#ff2222 ctermfg=1

highlight link GitGutterChangeLine DiffText


" ale
let g:ale_cpp_cc_options = "--std=c++20 -Wall"
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['eslint'],
\}
let b:ale_fixers = ['eslint']
let b:ale_fix_on_save = 1 " ?


" }}}
"


nmap gr <Plug>(Translate)
vmap t <Plug>(VTranslate)
